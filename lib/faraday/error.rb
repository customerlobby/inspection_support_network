require 'faraday'

module FaradayMiddleware
  class Error < Faraday::Response::Middleware
    # ISN APIs throw error message in success response i.e. 200 ok
    # so, we need to handle them explicitly by checking the status node
    # inside the response body
    def on_complete(env)
      raise Faraday::ParsingError, env.body.message if env.body.status == 'error'
    end
  end
end
