require 'faraday'
require 'faraday_middleware'
require 'active_support/all'
require 'inspection_support_network/version'

Dir[File.expand_path('../faraday/*.rb', __dir__)].each { |f| require f }
require File.expand_path('inspection_support_network/configuration', __dir__)
require File.expand_path('inspection_support_network/api', __dir__)
require File.expand_path('inspection_support_network/client', __dir__)
require File.expand_path('inspection_support_network/error', __dir__)

module InspectionSupportNetwork
  extend Configuration

  # Alias for InspectionSupportNetwork::Client.new
  # @return [InspectionSupportNetwork::Client]
  def self.client(options = {})
    InspectionSupportNetwork::Client.new(options)
  end

  # Delegate to InspectionSupportNetwork::Client
  def self.method_missing(method, *args, &block)
    return super unless client.respond_to?(method)
    client.send(method, *args, &block)
  end
end
