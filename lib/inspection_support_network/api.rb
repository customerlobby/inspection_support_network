require File.expand_path('request', __dir__)
require File.expand_path('response', __dir__)
require File.expand_path('connection', __dir__)

module InspectionSupportNetwork
  class API
    attr_accessor *Configuration::VALID_OPTIONS_KEYS

    def initialize(options = {})
      options = InspectionSupportNetwork.options.merge(options)
      Configuration::VALID_OPTIONS_KEYS.each do |key|
        send("#{key}=", options[key])
      end
    end

    def config
      conf = {}
      Configuration::VALID_OPTIONS_KEYS.each do |key|
        conf[key] = send key
      end
      conf
    end

    def company_name
      test_mode ? 'test' : @company_name
    end

    include Request
    include Response
    include Connection
  end
end
