module InspectionSupportNetwork
  # Wrapper for the InspectionSupportNetwork REST API.
  class Client < API
    Dir[File.expand_path('client/*.rb', __dir__)].each { |f| require f }

    include InspectionSupportNetwork::Client::Clients
    include InspectionSupportNetwork::Client::Orders
    include InspectionSupportNetwork::Client::Users
  end
end
