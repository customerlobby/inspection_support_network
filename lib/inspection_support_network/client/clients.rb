module InspectionSupportNetwork
  class Client
    # Methods for the Clients API
    module Clients
      #
      # Client List Actions
      #

      # Get a list of clients
      #
      # @param params [Hash] after date
      # @return Hash
      def clients(params = {})
        get('clients', params)
      end

      # Get a searched list of clients
      #
      # @param params [Hash] search query
      # @return Hash

      def clients_search(params = {})
        get('clients/search', params)
      end

      #
      # Individual Client Actions
      #

      # Get a single client
      #
      # @param id [String] UUID of the client
      # @param params [Hash] optional parameters to send
      # @return Hash
      def client(id, params = {})
        get("client/#{id}", params)
      end

      # Delete a single client
      #
      # @param id [String] UUID of the client
      # @param params [Hash] optional parameters to send
      # @return Hash
      def delete_client(id, params = {})
        delete("client/#{id}", params)
      end
    end
  end
end
