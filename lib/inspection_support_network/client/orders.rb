module InspectionSupportNetwork
  class Client
    # Methods for the Orders API
    module Orders
      #
      # Order List Actions
      #

      # Get a list of orders
      #
      # @param params [Hash] after_date or optional parameters
      # @return Hash
      def orders(params = {})
        get('orders', params)
      end

      # Get a searched list of orders
      #
      # @param params [Hash] search or optional parameters
      # @return Hash
      def orders_search(params = {})
        get('orders/search', params)
      end

      # Get a list of order types
      #
      # @param params [Hash] optional parameters
      # @return Hash
      def order_types(params = {})
        get('ordertypes', params)
      end

      #
      # Individual Order Actions
      #

      # Get a single order
      #
      # @param id [String] UUID of the order
      # @param params [Hash] optional parameters
      # @return Hash
      def order(id, params = {})
        get("order/#{id}", params)
      end

      # Get the webdelivery URL of a single order
      #
      # @param id [String] UUID of the order
      # @param params [Hash] optional parameters
      # @return Hash
      def order_webdelivery_url(id, params = {})
        get("order/webdelivery/#{id}", params)
      end

      # Mark a single order as complete
      #
      # @param id [String] UUID of the order
      # @param params [Hash] optional parameters
      # @return Hash
      def complete_order(id, params = {})
        post("order/complete/#{id}", params)
      end
    end
  end
end
