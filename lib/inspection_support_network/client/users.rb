module InspectionSupportNetwork
  class Client
    # Methods for the Users API
    module Users
      # Get details of authenticated user
      #
      def me(params = {})
        get('me', params)
      end

      # Get a list of users
      #
      def users(params = {})
        get('users', params)
      end

      # Get a single user
      #
      # @param id [String] UUID of the client
      def user(id, params = {})
        get("user/#{id}", params)
      end
    end
  end
end
