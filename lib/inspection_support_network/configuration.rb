module InspectionSupportNetwork
  module Configuration
    VALID_OPTIONS_KEYS = %i[
      adapter
      company_name
      domain
      endpoint
      password
      test_mode
      username
    ].freeze

    # Use the default Faraday adapter.
    DEFAULT_ADAPTER = Faraday.default_adapter

    # By default use the test API.
    DEFAULT_COMPANY_NAME = 'test'.freeze

    # By default use the main api URL.
    DEFAULT_DOMAIN = 'https://inspectionsupport.net'.freeze

    # By default use the default endpoint.
    DEFAULT_ENDPOINT = 'rest'.freeze

    DEFAULT_TEST_MODE = false

    attr_accessor *VALID_OPTIONS_KEYS

    # Convenience method to allow configuration options to be set in a block
    def configure
      yield self
    end

    def options
      VALID_OPTIONS_KEYS.inject({}) do |option, key|
        option.merge!(key => send(key))
      end
    end

    # When this module is extended, reset all settings.
    def self.extended(base)
      base.class_eval do
        def self.company_name
          @test_mode ? 'test' : @company_name
        end
      end
      base.reset
    end

    # Reset all configuration settings to default values.
    def reset
      self.adapter      = DEFAULT_ADAPTER
      self.company_name = DEFAULT_COMPANY_NAME
      self.domain       = DEFAULT_DOMAIN
      self.endpoint     = DEFAULT_ENDPOINT
      self.test_mode    = DEFAULT_TEST_MODE
    end
  end
end
