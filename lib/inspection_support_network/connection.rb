require 'faraday_middleware'
Dir[File.expand_path('../faraday/*.rb', __dir__)].each { |f| require f }

module InspectionSupportNetwork
  module Connection
    private

    def connection
      options = {
        url: [domain, company_name, endpoint].join('/')
      }

      Faraday::Connection.new(options) do |connection|
        connection.use FaradayMiddleware::Error
        connection.use FaradayMiddleware::Mashify
        connection.use FaradayMiddleware::FollowRedirection
        connection.use Faraday::Response::ParseJson
        connection.adapter(adapter)
      end
    end
  end
end
