module InspectionSupportNetwork
  # Defines HTTP request methods
  module Request
    # Perform an HTTP GET request
    def get(path, options = {})
      request(:get, path, options)
    end

    # Perform an HTTP POST request
    def post(path, options = {})
      request(:post, path, options)
    end

    # Perform an HTTP DELETE request
    def delete(path, options = {})
      request(:delete, path, options)
    end

    # Get the URL that would be requested for the given options
    #
    # @param path [String] the path of the REST action
    # @param options [Hash] query parameters
    # @return [String]
    def url_for(path = nil, options = {})
      base = [domain, company_name, endpoint, path].compact.join('/')
      joint = base.include?('?') ? '&' : '?'
      base << joint << sanitize(authentication_params.merge(options)).to_query if options.any?
      base
    end

    private

    # Perform an HTTP request
    def request(method, path, options)
      options = sanitize(options.merge(authentication_params))
      begin
        response = connection.send(method) do |request|
          case method
          when :get, :delete
            request.url(path, options)
          when :post, :put
            request.headers['Content-Type'] = 'application/json'
            request.body = options.to_json unless options.empty?
            request.url(path)
          end
        end
      rescue Faraday::ConnectionFailed => e
        raise ConnectionError, e
      rescue Faraday::TimeoutError => e
        raise TimeoutError, e
      rescue Faraday::ResourceNotFound => e
        raise NotFoundError, e
      rescue Faraday::ParsingError => e
        if e.message.include?('You must login to use this service')
          raise UnauthorizedError, 'Username/Password is incorrect'
        elsif e.message.include?('not found') || e.message.include?('could not locate')
          raise NotFoundError, e
        else
          raise ParsingError, e
        end
      rescue Faraday::SSLError => e
        raise SSLError, e
      rescue StandardError => e
        raise Error, e
      end

      Response.create(response.body)
    end

    OPTIONS_WHITELIST = %i[address after datetime password username q reportnumber].freeze

    # Format and sanitize the options before you send them off
    #
    # @param options [Hash] request options to sanitize
    # @return [Hash]
    def sanitize(options)
      return if options.blank?
      safe_options = {}
      OPTIONS_WHITELIST.each do |field|
        safe_options[field] = format_field(options[field]) if options.key?(field)
      end
      safe_options
    end

    # Format the fields to a format that the Protractor likes
    # @param value [Array, Date, or String] value to be formatted
    # @return String
    def format_field(value)
      if value.instance_of?(Array)
        value.join(',')
      elsif value.respond_to?(:strftime)
        value.strftime('%Y-%m-%d %H:%M:%S')
      elsif value.instance_of?(String)
        value
      end
    end

    # Authentication params sent with every request
    # @return Hash
    def authentication_params
      { username: username, password: password }
    end
  end
end
