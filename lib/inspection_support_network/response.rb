module InspectionSupportNetwork
  module Response
    def self.create(response_hash)
      data = begin
               response_hash.data.dup
             rescue StandardError
               response_hash
             end
      data.extend(self)
      data
    end
  end
end
