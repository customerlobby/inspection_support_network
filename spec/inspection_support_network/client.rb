require 'spec_helper'

RSpec.describe InspectionSupportNetwork::Client do
  it 'should connect using the configured endpoint and api version' do
    client = InspectionSupportNetwork.client
    endpoint = URI.parse([client.domain, client.api_version, client.endpoint].join('/'))
    connection = client.send(:connection).build_url(nil).to_s
    expect(connection).to eq(endpoint.to_s)
  end

  it 'should connect using the configured endpoint and api version in test mode' do
    client = InspectionSupportNetwork.client(test_mode: true)
    endpoint = URI.parse([client.domain, 'test', client.endpoint].join('/'))
    connection = client.send(:connection).build_url(nil).to_s
    expect(connection).to eq(endpoint.to_s)
  end
end
