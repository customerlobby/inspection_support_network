require 'spec_helper'

RSpec.describe InspectionSupportNetwork::Client::Clients do
  before do
    @client = authenticated_client
  end

  describe 'clients' do
    before do
      stub_request_with_fixture('clients/list', 'clients')
      stub_request_with_fixture('clients/after_date', 'clients',
                                after: Time.new(2015, 05, 01))
      stub_request_with_fixture('clients/search', 'clients/search',
                                q: 'Steve')
    end

    it 'should return a list of clients' do
      @client.clients
      expect(a_get('clients')).to have_been_made
    end

    it 'should return a list of clients filtered by date' do
      @client.clients(after: Time.new(2015, 05, 01))
      expect(a_get('clients', after: Time.new(2015, 05, 01))).to have_been_made
    end

    it 'should return a list of clients matching a search query' do
      @client.clients_search(q: 'Steve')
      expect(a_get('clients/search', q: 'Steve')).to have_been_made
    end
  end

  describe 'client' do
    before do
      stub_request_with_fixture('clients/lookup', 'client/433e88a4-c66d-5015-89d7-f9bf99a0398a')
    end

    it 'should return a client given a client ID' do
      @client.client('433e88a4-c66d-5015-89d7-f9bf99a0398a')
      expect(a_get('client/433e88a4-c66d-5015-89d7-f9bf99a0398a')).to have_been_made
    end
  end

  describe 'delete_client' do
    before do
      stub_request(:delete, InspectionSupportNetwork.url_for('client/433e88a4-c66d-5015-89d7-f9bf99a0398a', authentication_params))
        .to_return(body: fixture('clients/deletion'), headers: { content_type: 'application/json' })
    end

    it 'should delete a client given a client ID' do
      @client.delete_client('433e88a4-c66d-5015-89d7-f9bf99a0398a')
      request = a_request(:delete, InspectionSupportNetwork.url_for('client/433e88a4-c66d-5015-89d7-f9bf99a0398a', authentication_params))
      expect(request).to have_been_made
    end
  end
end
