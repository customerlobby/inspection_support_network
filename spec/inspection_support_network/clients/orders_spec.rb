require 'spec_helper'

search_params = {
  address: '1573 W. Buffington St',
  reportnumber: '172151573-W-BUFFINGTON-ST-UPLAND-CA-91784',
  datetime: Time.new(2015, 3, 2, 8)
}

RSpec.describe InspectionSupportNetwork::Client::Orders do
  before do
    @client = authenticated_client
  end

  describe 'orders' do
    before do
      stub_request_with_fixture('orders/list', 'orders')
      stub_request_with_fixture('orders/search', 'orders/search', search_params)
    end

    it 'should return a list of orders' do
      orders = @client.orders
      expect(a_get('orders')).to have_been_made
      expect(orders.orders.first.id).to eq('c95937a6-4441-4807-9dce-e0db7c3da0a9')
    end

    it 'should return a list of orders filtered by search params' do
      orders = @client.orders_search(search_params)
      expect(a_get('orders/search', search_params)).to have_been_made
      expect(orders.orders.first.id).to eq('c95937a6-4441-4807-9dce-e0db7c3da0a9')
    end
  end

  describe 'order_types' do
    before do
      stub_request_with_fixture('orders/types', 'ordertypes')
    end

    it 'should return a list of order types' do
      @client.order_types
      expect(a_get('ordertypes')).to have_been_made
    end
  end

  describe 'order' do
    before do
      stub_request_with_fixture('orders/lookup', 'order/c95937a6-4441-4807-9dce-e0db7c3da0a9')
    end

    it 'should return an order' do
      @client.order('c95937a6-4441-4807-9dce-e0db7c3da0a9')
      expect(a_get('order/c95937a6-4441-4807-9dce-e0db7c3da0a9')).to have_been_made
    end
  end

  describe 'order_webdelivery_url' do
    before do
      stub_request_with_fixture('orders/webdelivery_url', 'order/webdelivery/c95937a6-4441-4807-9dce-e0db7c3da0a9')
    end

    it 'should return the webdelivery url of an order' do
      @client.order_webdelivery_url('c95937a6-4441-4807-9dce-e0db7c3da0a9')
      expect(a_get('order/webdelivery/c95937a6-4441-4807-9dce-e0db7c3da0a9')).to have_been_made
    end
  end

  describe 'complete_order' do
    before do
      stub_post('order/complete/c95937a6-4441-4807-9dce-e0db7c3da0a9').to_return(
        body: fixture('orders/complete'),
        headers: { content_type: 'application/json' }
      )
    end

    it 'should complete an order' do
      @client.complete_order('c95937a6-4441-4807-9dce-e0db7c3da0a9',
                             reportnumber: '172151573-W-BUFFINGTON-ST-UPLAND-CA-91784')
      expect(a_post('order/complete/c95937a6-4441-4807-9dce-e0db7c3da0a9')).to have_been_made
    end
  end
end
