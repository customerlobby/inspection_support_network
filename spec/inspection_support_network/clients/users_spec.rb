require 'spec_helper'

RSpec.describe InspectionSupportNetwork::Client::Users do
  before do
    @client = authenticated_client
  end

  describe 'users' do
    before do
      stub_request_with_fixture('users/list', 'users')
    end

    it 'should return a list of users' do
      @client.users
      expect(a_get('users')).to have_been_made
    end
  end

  describe 'user' do
    before do
      stub_request_with_fixture('users/lookup', 'user/d0feb7f3-5e83-5a2f-b1ba-3634d1f6cad8')
    end

    it 'should return a user given ID' do
      @client.user('d0feb7f3-5e83-5a2f-b1ba-3634d1f6cad8')
      expect(a_get('user/d0feb7f3-5e83-5a2f-b1ba-3634d1f6cad8')).to have_been_made
    end
  end

  describe 'current_user' do
    before do
      stub_request_with_fixture('users/me', 'me')
    end

    it 'should return the authenticated user' do
      @client.me
      expect(a_get('me')).to have_been_made
    end
  end
end
