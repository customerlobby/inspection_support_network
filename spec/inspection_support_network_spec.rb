require 'spec_helper'

RSpec.describe InspectionSupportNetwork do
  after :each do
    InspectionSupportNetwork.reset
  end

  describe '.client' do
    it 'should be a InspectionSupportNetwork::Client' do
      expect(InspectionSupportNetwork.client).to be_a(InspectionSupportNetwork::Client)
    end
  end

  describe '#company_name' do
    it 'should return the default company name' do
      expect(InspectionSupportNetwork.company_name).to eq(InspectionSupportNetwork::Configuration::DEFAULT_COMPANY_NAME)
    end

    it 'should return the test company name when in test mode' do
      InspectionSupportNetwork.test_mode = true
      InspectionSupportNetwork.company_name = 'not test'
      expect(InspectionSupportNetwork.company_name).to eq('test')
    end
  end

  describe '#company_name=' do
    it 'should set the company name' do
      InspectionSupportNetwork.company_name = 'testco'
      expect(InspectionSupportNetwork.company_name).to eq('testco')
    end
  end

  describe '#adapter' do
    it 'should return the default adapter' do
      expect(InspectionSupportNetwork.adapter).to eq(InspectionSupportNetwork::Configuration::DEFAULT_ADAPTER)
    end
  end

  describe '#adapter=' do
    it 'should set the adapter' do
      InspectionSupportNetwork.adapter = :typhoeus
      expect(InspectionSupportNetwork.adapter).to eq(:typhoeus)
    end
  end

  describe '#endpoint' do
    it 'should return the default endpoint' do
      expect(InspectionSupportNetwork.endpoint).to eq(InspectionSupportNetwork::Configuration::DEFAULT_ENDPOINT)
    end
  end

  describe '#endpoint=' do
    it 'should set the endpoint' do
      InspectionSupportNetwork.endpoint = 'http://www.google.com'
      expect(InspectionSupportNetwork.endpoint).to eq('http://www.google.com')
    end
  end

  describe '#username=' do
    it 'should set the username' do
      InspectionSupportNetwork.username = 'cl_tech'
      expect(InspectionSupportNetwork.username).to eq('cl_tech')
    end
  end

  describe '#password=' do
    it 'should set the endpoint' do
      InspectionSupportNetwork.password = '321321'
      expect(InspectionSupportNetwork.password).to eq('321321')
    end
  end

  describe '#test_mode=' do
    it 'should set the test mode to true' do
      InspectionSupportNetwork.test_mode = true
      expect(InspectionSupportNetwork.test_mode).to eq(true)
    end

    it 'should set the test mode to false' do
      InspectionSupportNetwork.test_mode = false
      expect(InspectionSupportNetwork.test_mode).to eq(false)
    end
  end

  describe '#configure' do
    InspectionSupportNetwork::Configuration::VALID_OPTIONS_KEYS.each do |key|
      it "should set the #{key}" do
        InspectionSupportNetwork.configure do |config|
          # config.send("#{key}=", key)
          # expect(InspectionSupportNetwork.send(key)).to eq(key)
        end
      end
    end
  end
end
